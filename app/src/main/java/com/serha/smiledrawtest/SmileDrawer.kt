package com.serha.smiledrawtest

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.view.View

//import java.awt.font.ShapeGraphicAttribute.STROKE


class SmileDrawer(context: Context, var currentMood: Int) : View(context) {

    companion object {
        const val SkullRadiusToEyeOffset = 3
        const val SkullRadiusToEyeRadius = 10
        const val SkullRadiusToMouthWidth = 1
        const val SkullRadiusToMouthHeight = 2
        const val SkullRadiusToMouthOffset = 2
    }

    var paint: Paint? = null
    var path: Path? = null

    var strokeWidth = 25.toFloat()

    init {
        paint = Paint()

        paint?.style = Paint.Style.STROKE
    }

    override fun onDraw(canvas: Canvas) {
//        super.onDraw(canvas)
        canvas.drawARGB(80, 102, 204, 255)

        paint?.color = getColor(currentMood)
        paint?.strokeWidth = strokeWidth

//        path?.moveTo(34.toFloat(), 259.toFloat())
//        path?.cubicTo(68.toFloat(), 151.toFloat(), 286.toFloat(), 350.toFloat(), 336.toFloat(), 252.toFloat())
//        canvas.drawPath(path, paint)

        val radius = if (width > height) {
            height / 2 * 0.9
        } else {
            width / 2 * 0.9
        }
        canvas.drawCircle((width / 2).toFloat(), (height / 2).toFloat(), radius.toFloat(), paint)

        val eyeRadius = radius / SkullRadiusToEyeRadius
        val eyeOffset = radius / SkullRadiusToEyeOffset

        canvas.drawCircle(
            (width / 2).toFloat() - eyeOffset.toFloat(),
            (height / 2).toFloat() - eyeOffset.toFloat(),
            eyeRadius.toFloat(),
            paint
        )
        canvas.drawCircle(
            (width / 2).toFloat() + eyeOffset.toFloat(),
            (height / 2).toFloat() - eyeOffset.toFloat(),
            eyeRadius.toFloat(),
            paint
        )

        path = Path()

        val mouthWidth = radius / SkullRadiusToMouthWidth
        val mouthOffset = radius / SkullRadiusToMouthOffset
        val mouthHeight = radius / SkullRadiusToMouthHeight

        val startMouthX = (width / 2).toFloat() - mouthOffset.toFloat()
        val startMouthY = (height / 2).toFloat() + mouthOffset.toFloat()
        val endMouthX = (width / 2).toFloat() + mouthOffset.toFloat()
        val endMouthY = (height / 2).toFloat() + mouthOffset.toFloat()

        if (currentMood == 50) {
            path?.moveTo(startMouthX, startMouthY)
            path?.lineTo(endMouthX, endMouthY)
            canvas.drawPath(path, paint)
        } else if (currentMood > 50) {
            val currentHeight = (mouthHeight / 100) * (currentMood - 50)
            val x = (width / 2).toFloat()
            val y = startMouthY + currentHeight.toFloat()
            path?.moveTo(startMouthX, startMouthY)
            path?.quadTo(x, y, endMouthX, endMouthY)
            canvas.drawPath(path, paint)
        } else if (currentMood < 50) {
            val currentHeight = (mouthHeight / 100) * (50 - currentMood)
            val x = (width / 2).toFloat()
            val y = startMouthY - currentHeight.toFloat()
            path?.moveTo(startMouthX, startMouthY)
            path?.quadTo(x, y, endMouthX, endMouthY)
            canvas.drawPath(path, paint)
        }
    }

    fun getColor(mood: Int): Int {
        var redMin: Float = 0.8.toFloat()
        var greenMin: Float = 0.16.toFloat()
        var blueMin: Float = 0.16.toFloat()

        var redMiddle: Float = 0.8.toFloat()
        var greenMiddle: Float = 0.59.toFloat()
        var blueMiddle: Float = 0.16.toFloat()

        var redMax: Float = 0.38.toFloat()
        var greenMax: Float = 0.6.toFloat()
        var blueMax: Float = 0.25.toFloat()

        var redColor = if (mood > 50) {
            redMiddle + (redMax - redMiddle) * ((mood.toFloat() - 50.toFloat()) / 50.toFloat())
        } else {
            redMin + (redMiddle - redMin) * ((mood.toFloat()) / 50.toFloat())
        }

        var greenColor = if (mood > 50) {
            greenMiddle + (greenMax - greenMiddle) * ((mood.toFloat() - 50.toFloat()) / 50.toFloat())
        } else {
            greenMin + (greenMiddle - greenMin) * ((mood.toFloat()) / 50.toFloat())
        }

        var blueColor = if (mood > 50) {
            blueMiddle + (blueMax - blueMiddle) * ((mood.toFloat() - 50.toFloat()) / 50.toFloat())
        } else {
            blueMin + (blueMiddle - blueMin) * ((mood.toFloat()) / 50.toFloat())
        }

        return Color.rgb((redColor * 255).toInt(), (greenColor * 255).toInt(), (blueColor * 255).toInt())
    }
}