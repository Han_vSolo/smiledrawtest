package com.serha.smiledrawtest

import android.os.Bundle
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var currentMood = 75
    private var smileDrawer: SmileDrawer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(SmileDrawer(this))
        setContentView(R.layout.activity_main)

        initSmileDrawer()

        initSlider()
    }

    private fun initSlider() {
        val max = 100
        val min = 0
        val total = max - min

        fluidSlider.positionListener = { pos ->
            fluidSlider.bubbleText = "${min + (total * pos).toInt()}"
            currentMood = min + (total * pos).toInt()
            smileDrawer?.currentMood = currentMood
            smileDrawer?.invalidate()
            fluidSlider.colorBar = smileDrawer!!.getColor(currentMood)
        }
        fluidSlider.position = (currentMood.toFloat() / 100)
        fluidSlider.startText = ""
        fluidSlider.endText = ""
    }

    private fun initSmileDrawer() {
        smileDrawer = SmileDrawer(this, currentMood)
        val params =
            RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP)
        params.setMargins(0, 0, 0, 120)
        smileDrawer?.layoutParams = params

        mainRelativeLayout.addView(smileDrawer)
    }
}
